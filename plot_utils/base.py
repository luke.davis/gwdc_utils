# Copyright (C) 2020-2021 Alex Codoreanu
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# General plot parameters
label_font = dict(fontname='cmr10', fontweight='bold', fontsize=24)

title_font = dict(fontname='cmr10', fontsize=20)

xtick_style = dict(fontsize=18)

ytick_style = dict(fontsize=18)

marker_style = dict(markersize=10,
                    markeredgecolor='k',
                    fillstyle='full',
                    alpha=0.5,
                    markerfacecolor='red')

figure_style = dict(figsize=(9, 9), dpi=100)

background_color = 'tab:gray'
