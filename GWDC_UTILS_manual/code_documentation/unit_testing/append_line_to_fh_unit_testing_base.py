def append_line_to_fh(logfile_fh, line, verbose):
    """Append a line to the given file handle, optionally printing it.

    @param logfile_fh: file handle
    @param line: line to print
    @param verbose: whether to print to stdout as well
    """
