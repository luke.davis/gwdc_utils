def append_line_to_file(local_log_file, line, verbose):
    """Append line to existing file.

    @param local_log_file: file to append to
    @param line: line to append
    @param verbose: bool to print line to device
    @return:
    """
