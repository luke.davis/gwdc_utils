def get_line_elements(local_temp_line, temp_element, split_element=None):
    """This function returns a list object containing all elements in local_temp_line after
    temp_element is deleted. The list is created by splitting local_temp_line by split_element.

    @param local_temp_line: string object to be parsed.
    @param temp_element: element to be deleted from string.
                         ALL INSTANCES WILL BE DELETED.
    @param split_element: string element to split the input line by.
                          Default value is ', '

    @return: local_temp_line.split(split_element)
    """
