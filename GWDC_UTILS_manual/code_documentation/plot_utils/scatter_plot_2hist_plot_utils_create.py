def scatter_plot_2hist(data,
                       x_variables=None,
                       y_variables=None,
                       label_variables=None,
                       output_file_name=None,
                       plot_title=None,
                       ylabel=None,
                       xlabel=None,
                       min_x=None,
                       max_x=None,
                       min_y=None,
                       max_y=None,
                       max_x_counts=None,
                       max_y_counts=None,
                       xlog=False,
                       ylog=False,
                       xbinwidth=0.05,
                       ybinwidth=0.1,
                       grid=True,
                       verbose=False):
    """This is a generic function that creates a combination scatter and associated
    histogram for each variable. It takes in a DataFrame that holds multiple x/y columns.
    These columns (keys) need to be identified and passed as list objects, even if it's
    a list with a single item.

    Adapted from code provided by Dr. Fiona Panther.

    USAGE:
        from pandas import read_csv
        from plot_utils.create import scatter_plot_2hist

        test_path = '/fred/oz996/PyCharm/alexc/spiir_testing/plot_utils_dev'
        data = read_csv('{}/postcoh__summary.csv'.format(test_path))

        output_file_name = 'test.png'
        snr_variables = ['snglsnr_L', 'snglsnr_H', 'snglsnr_V']
        chi2_variables = ['chisq_L', 'chisq_H', 'chisq_V']
        label_variables = ['LIGO-Livingston', 'LIGO-Hanford', 'LIGO-Virgo']

        scatter_plot_2hist(data, x_variables=snr_variables, y_variables=chi2_variables, label_variables=label_variables,
                           output_file_name='test.png', plot_title='My test scatter plot', ylabel="Chi$^2$",
                           xlabel="SNR", verbose=True, grid=True, min_x=4, max_x=50, min_y=1, max_y=100, xlog=True,
                           ylog=True, max_y_counts=100, max_x_counts=200)

    :param data: REQUIRED DataFrame object with keys containing the x/y _variables
    :param x_variables: REQUIRED list of DataFrame column names that will be the x-axis data
    :param y_variables: REQUIRED list of DataFrame column names that will be the y-axis data
    :param label_variables: REQUIRED list of labels for the plot legend
    :param output_file_name: OPTIONAL string of output name
    :param plot_title: OPTIONAL string of plot title
    :param ylabel: OPTIONAL string of y-axis label
    :param xlabel: OPTIONAL string of x-axis label
    :param min_x: OPTIONAL numerical value for min x value
    :param max_x: OPTIONAL numerical value for max x value
    :param min_y: OPTIONAL numerical value for min y value
    :param max_y: OPTIONAL numerical value for max y value
    :param xlog: OPTIONAL bool which turns x-axis to log scale
    :param ylog: OPTIONAL bool which turns y-axis to log scale
    :param max_x_counts: OPTIONAL numerical value for max x value histogram
    :param max_y_counts: OPTIONAL numerical value for max y value histogram
    :param xbinwidth: OPTIONAL numerical value for x-axis bin value for histogram plot
                      DEFAULT value is set 0.05.
    :param ybinwidth: OPTIONAL numerical value for y-axis bin value for histogram plot
                      DEFAULT value is set 0.1.
    :param grid: OPTIONAL bool to
    :param verbose: OPTIONAL
    :return:

    """
