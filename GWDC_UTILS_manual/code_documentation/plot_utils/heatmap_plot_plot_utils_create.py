def heatmap_plot(data,
                 x,
                 y,
                 z,
                 plot_name=None,
                 plot_title=None,
                 color_map=None):
    """ This is a generic heatmap plot generator.

    USAGE:
        from pandas import read_csv
        from plot_utils.create import heatmap_plot

        test_path = '/fred/oz996/PyCharm/alexc/spiir_testing/plot_utils_dev/run_plots_dev'
        data = read_csv('{}/dataframes/df_latency_30.csv'.format(test_path))
        x = 'detected_snr_H'
        y = 'detected_snr_L'
        z = 'detected_snr_V'
        plot_title = 'heatmap_test.png'
        plot_name = plot_title
        color_map = 'YlGnBu'

        heatmap_plot(data, x, y, z, plot_title, plot_name, color_map=color_map)

    :param data: A dataframe object containing columns x, y, z
    :param x: column name for x-axis data
    :param y: column name for y-axis data
    :param z: column name for z-axis data
    :param plot_title: string denoting the plot title
    :param plot_name: string denoting the output name for the saved object
    :param color_map: string colormap identifier
    
    :return:
    """
