def get_approximant(bank_id):
    """ This function returns the appropriate approximant based on the bank number.
    The logic it implements is:
        if bank_id < 90 then approximant = 'SpinTaylorT4'
        if bank_id > 89 && bank_id < 100 then approximant = 'SEOBNRv4_ROM'
        if bank id > 99 && bank_id < 112 then approximant = 'SpinTaylorT4'
        if bank_id > 111 then approximant = 'SEOBNRv4_ROM'

    :param bank_id: numerical value that can be cast to int
    :return approximant: string name of approximant file
    """
