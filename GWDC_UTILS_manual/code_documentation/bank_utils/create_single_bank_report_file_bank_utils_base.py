def create_single_bank_report_file(
        dir_path,
        report_file_string='logs_*',
        report_file_type='csv',
        match_final_threshold=0.9,
        output_file_id='combined_data_report_for_all_logs.csv',
        stats_file_id='template_bank_report_default.csv',
        bad_match_final_data_output_id='templates_to_be_rerun.csv'):
    """This function combines all the single outputs from create_err_status_file into a single file.

    usage:
        from bank_utils.base import create_single_bank_report_file
        create_single_bank_report_file('/fred/oz016/PSD-1263344418-21600')
    @param dir_path:
    @param report_file_string:
    @param report_file_type:
    @param match_final_threshold:
    @param output_file_id:
    @param stats_file_id:
    @param bad_match_final_data_output_id:

    @return: 0
    """
