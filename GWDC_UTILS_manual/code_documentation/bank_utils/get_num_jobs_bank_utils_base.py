def get_num_jobs(num_directories, dir_path, num_jobs=4):
    """This function identifies the number of individual threads that will be required based on:
        - the number of identified directories
        - the number of job-threads available:
            - default is set to 4 as that is the polite limit available on the ozstar login nodes

    :param dir_path: the directory path to the location of the templates and logs
    :param num_directories: the number of logs_* directories in the dir_path
    :param num_jobs: the default number of jobs is set to 4. If you'd like to run more threads, make sure that
                      you make an appropriate request.
    :return: int(num_jobs)
    """
