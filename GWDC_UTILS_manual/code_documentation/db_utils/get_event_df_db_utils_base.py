def get_event_df(pg_client, event_name):
    """This function returns a full event dataframe.

    :param pg_client: client connection to GraceDb
    :param event_name: string event name
    :return: json of the GraceDb event object
    """
