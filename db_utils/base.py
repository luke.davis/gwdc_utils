# Copyright (C) 2020-2021 Alex Codoreanu
# Copyright (C) 2020-2021 Patrick Clearwater
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import datetime
from pandas.io.json import json_normalize
try:
    from ligo.gracedb.rest import GraceDb
except ImportError:
    print 'ligo-gracedb package not available'

O3b_replay_events_path = '/home/alex.codoreanu/PyCharm/spiir_testing/O3_replay__dev/replay_events'


def get_current_time_string():
    """This function returns a time string where the datetime output
    has underscores instead of local_el in ['-', ' ', ':', '.'].

    :return current time string:
    """
    # get time_stamp string
    current_time = str(datetime.datetime.now())
    for local_el in ['-', ' ', ':', '.']:
        current_time = current_time.replace(local_el, '_')

    return str(current_time)


def get_GraceDB_client(playground=False):
    """This function returns a connection to GraceDB.

    :param playground: if True the returned connection
                       will point to GraceDB-playground

    :return a connection to GraceDB
    """

    if not playground:
        return GraceDb()
    else:
        return GraceDb(service_url='https://gracedb-playground.ligo.org/api/')


def clean_gracedb_row(gracedb_row, poptypes_list=None):
    """This function cleans up a client dictionary by poping any
    key where type(dict[key]) is in poptypes_list. If no poptypes_list
    is specified then poptypes_list = [list, dict].

    :param gracedb_row: an event object from client.events()
    :return: DataFrame object with now [list, dict] items
    """
    if poptypes_list is None:
        poptypes_list = [dict, list]
    for key in gracedb_row.keys():
        # remove dictionary key if it's a list or dict
        if type(gracedb_row[key]) in poptypes_list:
            gracedb_row.pop(key)

    return DataFrame(gracedb_row, index=[0])


def get_event_df(pg_client, event_name):
    """This function returns a full event dataframe.

    :param pg_client: client connection to GraceDb
    :param event_name: string event name
    :return: json of the GraceDb event object
    """
    return json_normalize(pg_client.event(event_name).json())


def get_autocorr_and_latency(pg_client,
                             event_name,
                             autocorr_check=None,
                             ew_check=None,
                             verbose=False):
    """This function returns the autocorrelation length and latency variable
    of the bank responsible for the trigger.

    THE EXPECTATION IS THAT THE coinc.xml FILE HAS BEEN CREATED AND THAT:
    1. aucorrelation length is identified just before the early warning flag
       in the bank id submission flag.
       Here's an expectation of the format:
       "/home/manoj.kovalam/O3b/O3_Replay/
       banks/autocorr_1051/EW_50/iir_H1-GSTLAL_SPLIT_BANK_0006-a1-0-0.xml.gz"

    The current implementation pulls the coinc.xml file associated with the event,
    casts it a string. A 50 element subset is extracted by first finding the location
    of the following string element:
        "/autocorr_{autocorrelation length}/...".

    The autocorrelation length string is then found by finding the following "/" and removing "autocorr_".

    Following the extraction of the autocorrelation string, the remaining string should contain the
    latency string in the following format:
        "ew_{latency integer value}/.."

    The latency integer value is extracted by dropping "ew_" from the substring preceding the first "/"
    character.

    USAGE:

        from db_utils.base import get_GraceDB_client
        from db_utils.base import get_autocorr_and_latency

        pg_client = get_GraceDB_client(playground=True)
        event_name = 'M299629'

        autocorr, latency = get_autocorr_and_latency(pg_client, event_name)

        print 'autocorrelation length: {}\nlatency: {}'.format(autocorr, latency)


    :param pg_client: client connection to GraceDb
    :param event_name: string event name
    :param autocorr_check: string identifier of the autocorrelation parameter in the banks path
    :param ew_check: string identifier of the latency parameter in the banks path

    :return: autocorr_string, ew_string
    """
    response = pg_client.files(event_name, 'coinc.xml')
    file_contents = response.read()
    if autocorr_check is None:
        autocorr_check = "/autocorr_"

    if ew_check is None:
        ew_check = "ew_"

    autocorr_index = file_contents.find(autocorr_check)
    if autocorr_index < 0:
        if verbose:
            print file_contents

    if verbose:
        print 'autocorr_index: {}'.format(autocorr_index)

    file_contents = str(file_contents[autocorr_index:autocorr_index +
                                      50]).lower()
    file_contents = file_contents[len(autocorr_check):]
    autocorr_string = str(file_contents[:file_contents.find('/')])
    file_contents = file_contents[file_contents.find('/') + 1:]
    file_contents = str(file_contents[:file_contents.find('/')])
    ew_string = file_contents.replace(ew_check, '')

    del file_contents, response, ew_check, autocorr_check, autocorr_index

    return autocorr_string, ew_string
